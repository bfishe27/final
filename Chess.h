#ifndef CHESS_H
#define CHESS_H

#include <iostream>
#include "Piece.h"
#include "Board.h"

class Chess
{
public:
	// This default constructor initializes a board with the standard piece positions, and sets the state to white's turn
	Chess( void );

	

	// Returns a constant reference to the board 
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	const Board& board( void ) const { return _board; }

	// Returns true if it's white's turn
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	bool turn_white( void ) const { return _turn_white; }

	// Attemps to make a move. If successfull, the move is made and the turn is switched white <-> black
	bool make_move( std::pair< char , char > start , std::pair< char , char > end );
	
	//so we could check stalemate and not have a const conflict
	bool test_path_clear(Board b, std::pair<char, char>, std::pair<char, int> ) const;

	// Returns true if the designated player is in check
	bool in_check( bool white )const;

	// Returns true if the designated player is in mate
	bool in_mate( bool white ) const;

	// Returns true if the designated player is in mate
	bool in_stalemate( bool white ) const;

	//to check stalemate and mate without having const errors
	bool test_move( Board b, std::pair<char, char> start, std::pair<char, char> end) const;

	//also for stalemate and mate to avoid const errors
	bool testCheck( Board b, bool white) const;

	//to check if the path is clear to see if its a legal move
	bool path_clear(std::pair<char,char>start, std::pair<char,int>lendir) const;

	//to get the length and direction to pass to path_clear
	std::pair<char,int> length_direction(std::pair< char , char > start , std::pair< char , char > end ) const;

	//to be able to use it to its best ability had to make it a friend, access to  _board
	friend std::istream& operator >> ( std::istream& is, Chess& chess );
	
private:
	// The board
	Board _board;

	// Is it white's turn?
	bool _turn_white;
};

// Writes the board out to a stream
std::ostream& operator << ( std::ostream& os , const Chess& chess );

// Reads the board in from a stream
std::istream& operator >> ( std::istream& is , Chess& chess );


#endif // CHESS_H
