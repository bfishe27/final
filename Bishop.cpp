//cpp file for Bishop
#include <utility>
#include "Bishop.h"
#include <cmath>

bool Bishop::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const{
  //checking if it moves vertical or horizontal
  if(start.first == end.first && start.second == end.second){
    return false;
  }
  //checking bounds
  if(start.first < 'A' || end.first < 'A' || start.first > 'H' || end.first > 'H'){
    return false;
  }
  if(start.second < '1' || end.second < '1' || start.second > '8' || end.second > '8'){
    return false;
  }
  //checking that it moves diagonal only
  if(abs(start.first - end.first) != abs(start.second - end.second)){
    return false;
  }
  return true;
}

bool Bishop::legal_capture_shape(std::pair<char, char> start, std::pair<char, char> end)const{
  return legal_move_shape(start, end);
  //legal capture shape is the same as legal move shape
}
