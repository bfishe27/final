//cpp file for Queen
#include <utility>
#include <cmath>
#include "Queen.h"


  
bool Queen::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const{

  bool valid = true;
  //check if made a move
  if(start.first == end.first && start.second == end.second){
    return false;
  }
  //check bounds
  if (start.first < 'A' || end.first < 'A' || start.first > 'H' || end.first > 'H') {
    return false;
  }
  if (start.second < '1' || end.second < '1' || start.second > '8' || end.second > '8') {
    return false;
  }
  //checks if moving backwards or diagonally
  if (abs(start.first - end.first) != abs(start.second - end.second)) {
    if (start.first != end.first && start.second == end.second) {
      valid = true;
    }
    //check if moving vertically
    else if (start.first == end.first && start.second != end.second) {
      valid = true;
    }
    else {
      valid = false;
    }
  }
  return valid;
}

bool Queen::legal_capture_shape(std::pair<char, char> start, std::pair<char, char> end)const{
  return legal_move_shape(start, end);
  //same moves
}
