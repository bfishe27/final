//cpp file for Rook
#include "Rook.h"

bool Rook::legal_move_shape( std::pair< char , char > start , std::pair< char \
 , char > end ) const
        {
	  //checks if a move was made
	  if ((start.first == end.first) && (start.second == end.second)) {
	    return false;
	  }
	  //checks bounds
            if (start.first < 'A' || end.first < 'A' || start.first > 'H' || end.first > 'H'\
)
	      {
		return false;
	      }
	    if (start.second < '1' || end.second < '1' || start.second > '8' || end.second > \
		'8') {
	      return false;
	    }
	    //checks if not moving horizontal or vertical

	    if (start.first != end.first) {
	      if (start.second != end.second) {
		return false;
	      }
	    }
	    if (start.second != end.second) {
	      if (start.first != end.first) {
		return false;
	      }
	    }
	    
          return true;
        }


bool Rook::legal_capture_shape( std::pair< char , char > start , std::pair< char , char > end ) const
        {
         	return legal_move_shape( start , end );
		//same as legal move shape
        }
