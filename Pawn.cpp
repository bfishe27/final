//cpp file for Pawn
#include "Pawn.h"
#include <utility>
#include <iostream>


bool Pawn::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const{

  bool valid = true;
  //checks bounds
  if (start.first < 'A' || end.first < 'A' || start.first > 'H' || end.first > 'H') {
    return false;
  }
  if (start.second < '1' || end.second < '1' || start.second > '8' || end.second > '8') {
    return false;
  }
  //checks if a move was made
  if (start.first == end.first && start.second == end.second) {
    valid = false;
  }
  //check if not moving forward only
  if (is_white() == true) {
    if (start.first != end.first) {
      valid = false;
    }
    //checks if moving two for first move
    else if (start.second == '2') {
      if (end.second > start.second + 2 || end.second < start.second) {
        valid = false;
      }
    }
    //checks if not moving forward
    else if ( start.second > end.second) {
      valid = false;
    }
  }
  //same as above but for black
  if (is_white() == false) {
    if (start.first != end.first) {
      valid = false;
    }
    else if (start.second == '7') {
      if (end.second < start.second - 2 || end.second > start.second) {
        valid = false;
      }
    }
    else if (start.second < end.second) {
      valid = false;
    }
  }
  return valid;
}

bool Pawn::legal_capture_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  bool valid = true;
  //checks if moving diagonally
  if (end.first != start.first + 1 && end.first != start.first - 1) {
    valid = false;
  }
  //checks if moving by 1 for white
  if (is_white() == true) {
    if ((end.first == start.first + 1 && end.second != start.second + 1) ||
	(end.first == start.first - 1 && end.second != start.second + 1)) {
      valid = false;
    }
  }
  //checks if moving by 1 for black
  else {
    if ((end.first == start.first + 1 && end.second != start.second - 1) ||
	(end.first == start.first - 1 && end.second != start.second - 1)) {
      valid = false;
    }
  }
  return valid;
}
