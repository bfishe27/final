//cpp file for King, implementations

#include "King.h"
#include <utility>

using std::pair;

bool King::legal_capture_shape(pair<char, char> start, pair<char, char> end) const {

  //calls legal move shape bc same moves
  return legal_move_shape(start, end);  
}

bool King::legal_move_shape(pair<char, char> start, pair<char, char> end) const{

  bool valid = true;
  //checks bounds
  if (start.first < 'A' || end.first < 'A' || start.first > 'H' || end.first > 'H') {
    return false;
  }
  if (start.second < '1' || end.second < '1' || start.second > '8' || end.second > '8') {
    return false;
  }
  //checks if not moving
  if (end.first == start.first && end.second == start.second) {
    valid = false;
  }
  //checking only moving one square
  if (end.first != start.first && end.first != start.first + 1 && end.first != start.first - 1) {
    valid = false;
  }
  if (end.second != start.second && end.second != start.second + 1 && end.second != start.second - 1) {
    valid = false;
  }
  return valid;

}
