#include "Chess.h"
#include <map>
#include <utility>
#include <cctype>
#include <vector>
#include <iostream>

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Chess::Chess( void ) : _turn_white( true )
{
  // Add the pawns
  for( int i=0 ; i<8 ; i++ )
    {
      _board.add_piece( std::pair< char , char >( 'A'+i , '1'+1 ) , 'P' );
      _board.add_piece( std::pair< char , char >( 'A'+i , '1'+6 ) , 'p' );
    }
  
  // Add the rooks
  _board.add_piece( std::pair< char , char >( 'A'+0 , '1'+0 ) , 'R' );
	_board.add_piece( std::pair< char , char >( 'A'+7 , '1'+0 ) , 'R' );
	_board.add_piece( std::pair< char , char >( 'A'+0 , '1'+7 ) , 'r' );
	_board.add_piece( std::pair< char , char >( 'A'+7 , '1'+7 ) , 'r' );
	
	// Add the knights
	_board.add_piece( std::pair< char , char >( 'A'+1 , '1'+0 ) , 'N' );
	_board.add_piece( std::pair< char , char >( 'A'+6 , '1'+0 ) , 'N' );
	_board.add_piece( std::pair< char , char >( 'A'+1 , '1'+7 ) , 'n' );
	_board.add_piece( std::pair< char , char >( 'A'+6 , '1'+7 ) , 'n' );
	
	// Add the bishops
	_board.add_piece( std::pair< char , char >( 'A'+2 , '1'+0 ) , 'B' );
	_board.add_piece( std::pair< char , char >( 'A'+5 , '1'+0 ) , 'B' );
	_board.add_piece( std::pair< char , char >( 'A'+2 , '1'+7 ) , 'b' );
	_board.add_piece( std::pair< char , char >( 'A'+5 , '1'+7 ) , 'b' );

	// Add the kings and queens
	_board.add_piece( std::pair< char , char >( 'A'+3 , '1'+0 ) , 'Q' );
	_board.add_piece( std::pair< char , char >( 'A'+4 , '1'+0 ) , 'K' );
	_board.add_piece( std::pair< char , char >( 'A'+3 , '1'+7 ) , 'q' );
	_board.add_piece( std::pair< char , char >( 'A'+4 , '1'+7 ) , 'k' );
}

//determines integer length of move to be made as well as direction in terms of letters spelled out in function.
std::pair<char,int> Chess::length_direction(std::pair< char , char > start , std::pair< char , char > end )const {
  int length;
  char dir;
  std:: pair<char, int>lendir;
  //check if direction up or down
  if(start.first == end.first) {
    if(end.second > start.second) {
      length = int(end.second-start.second);
      dir = 'n';
    }
    else {
      length = int(start.second-end.second);
      dir = 's';
    }
    lendir = std::make_pair(dir,length);
    return lendir;
  }
  //check if direction left or right
  if(start.second == end.second) {
    if (end.first > start.first) {
      length = int(end.first-start.first);
      dir = 'e';
    }
    else {
      length = int(start.first-end.first);
      dir = 'w';
    }
    lendir = std::make_pair(dir,length);
    return lendir;
  }
  //r for top right direction
  if((end.first > start.first) &&(end.second > start.second)) {
    length = int(end.first-start.first);
    dir = 'r';
  }
  //l for bottom left direction
  if((end.first < start.first) &&(end.second < start.second)) {
    length = int(end.first-start.first);
    dir	= 'l';
  }
  //b for bottom right direction
  if((end.first > start.first) &&(end.second < start.second)) {
    length = int(end.first-start.first);
    dir	= 'b';
  }
  //t for top left direction
  if((end.first < start.first) &&(end.second > start.second)) {
    length = int(start.first-end.first);
    dir	= 't';
  }
  lendir = std::make_pair(dir,length);
  return lendir;
}
//determine if there were any pieces in the path of the move
bool Chess::path_clear(std::pair<char, char> start, std::pair<char, int> lendir) const {
  std::pair<char, char> loc;
  char dir;
  int len;
  switch(lendir.first) {

  case 'n':
    //check if move goes off board, same design for all other cases
    if (start.second + lendir.second > '8') {
      return false;
    }
    //iterate through path to check if path is clear, same for design for all other directions
    for (int index = start.second + 1; index <= start.second + lendir.second - 1; index++) {
      loc = std::make_pair(start.first, index);
      if (_board(loc) != NULL) {
        return false;
      }
    }
    break;

  case 's':
    if ((start.second - lendir.second) < '1') {
      return false;
    }
    for (int index = start.second - 1; index >= start.second - lendir.second + 1; index--) {
      loc = std::make_pair(start.first, index);
      if (_board(loc) != NULL) {
        return false;
      }
    }
    break;

  case 'e':
    if ((start.first + lendir.second) > 'H') {
      return false;
    }
    for (char index = start.first + 1; index <= start.first + lendir.second - 1; index++) {
      loc = std::make_pair(index, start.second);
      if (_board(loc) != NULL) {
        return false;
      }
    }
    break;

  case 'w':
    if ((start.first - lendir.second) < 'A') {
      return false;
    }
    for (char index = start.first - 1; index >= start.first + lendir.second + 1; index++) {
      loc = std::make_pair(index, start.second);
      if (_board(loc) != NULL) {
        return false;
      }
    }
    break;

  case 'r':
    if (start.first + lendir.second > 'H') {
      return false;
    }
    for (int index = 1; index <= lendir.second - 1; index++) {
      dir = (start.first + index);
      len = (start.second + index);
      loc = std::make_pair(dir, len);
      if (_board(loc) != NULL) {
        return false;
      }
    }
    break;

  case 'b':
    if (start.second - lendir.second < '1') {
      return false;
    }
    for (int index = 1; index <= lendir.second - 1; index++) {
      dir = (start.first + index);
      len = (start.second - index);
      loc = std::make_pair(dir, len);
      if (_board(loc) != NULL) {
        return false;
      }
    }
    break;

  case 'l':
    if (start.second - lendir.second < '1') {
      return false;
    }
    for (int index = 1; index <= abs(lendir.second) - 1; index++) {
      dir = (start.first - index);
      len = (start.second - index);
      loc = std::make_pair(dir, len);
      if (_board(loc) != NULL) {
        return false;
      }
    }
    break;
    
  default:
    if (start.second + lendir.second > '8') {
      return false;
    }
    for (int index = 1; index <= lendir.second - 1; index++) {
      dir = (start.first - index);
      len = (start.second + index);
      loc = std::make_pair(dir, len);
      if (_board(loc) != NULL) {
        return false;
      }
    }
    break;
  }
  return true;  
}
//ensures that the move entered in is valid and the path is clear and the move doesn't result in a check
bool Chess::make_move( std::pair< char , char > start , std::pair< char , char > end )
{
  
  std::pair <char, int> len_dir;
  start.first = toupper(start.first);
  end.first = toupper(end.first);
  char endP;
  bool isPiece = false;
  //check if piece at start
  if (_board(start) == NULL) {
    return false;
  }
  //check correct turn
  if (_turn_white == true) {
    if (_board(start)->is_white() != true) {
      return false;
    }
  }
  //check correct turn
  if (_turn_white != true) {
    if (_board(start)->is_white() == true) {
      return false;
    }
  }
 
  //check that piece at end is opposite color
  if (_board(end) != NULL) {
    if (_board(end) -> is_white() == _board(start)->is_white()) {
      return false;
    }
    else {
      if (_board(start)->legal_capture_shape(start, end) == false) {
	return false;
      }
    }
  }
  else if (_board(start)->legal_move_shape(start, end) == false) {
    return false;
  }
  //check if no need for path_clear
  if (_board(start)->to_ascii() != 'N' || _board(start)->to_ascii() != 'n' ||
      _board(start)->to_ascii() != 'K' || _board(start)->to_ascii() != 'k') {
    len_dir = length_direction(start, end);
    //check if path is clear
    if (path_clear(start, len_dir) == false) {
      return false;
    }
  }
  //remove captured piece
  if (_board(end) != NULL) {
    isPiece = true;
    endP = _board(end)->to_ascii();
    _board.remove_piece(end);
  }
  //if pawn check if move possible
  if (_board(start)->to_ascii() == 'P' || _board(start)->to_ascii() == 'p') {
    if (_board(start)->legal_move_shape(start, end) == true) {
      if (isPiece == true) {
        return false;
      }
    }
    else if (_board(start)->legal_capture_shape(start, end) == true) {
      if (isPiece == false) {
        return false;
      }
    }
  }
  //move start piece to end
  _board.add_piece(end, _board(start)->to_ascii());
  //remove start piece from initial location
  _board.remove_piece(start);
  //check if move makes check
  if (in_check(_turn_white) == true) {
    _board.add_piece(start, _board(end)->to_ascii());
    if (isPiece == true) {
      _board.add_piece(end, endP);
    }
    else {
      _board.remove_piece(end);
    }
    return false;
  }
  //promote pawn to queen when it reaches end of board
  if (_board(end)->to_ascii() == 'P' || _board(end)->to_ascii() == 'p') {
    if (end.second == '8' || end.second == '1') {
      if (_turn_white) {
        _board.add_piece(end, 'Q');
      }
      else {
        _board.add_piece(end, 'q');
      }
    }
  }
  _turn_white = !_turn_white;
  return true;
}

bool Chess::in_check( bool white ) const
{
  std::pair<char, char> position;
  std::pair<char, char> king;
  std::vector<std::pair<char, char>> otherPieces;
  //iterate through board and collect vector of all opposing pieces
  for (char letter = 'A'; letter <= 'H'; letter++) {
    for (char num = '8'; num >= '1'; num--) {
      position = std::make_pair(letter, num);
      if (_board(position) != NULL) {
        if (_board(position)->is_white() != white){
	  otherPieces.push_back(position);
        }
        else if (_board(position)->to_ascii() == 'K' || _board(position)->to_ascii() == 'k') {
          king = position;
	}
      }
    }
  }
  //iterate through said vector and see if any valid moves exist from any piece to king
  for (std::vector<std::pair<char, char>>::const_iterator it = otherPieces.cbegin();
       it != otherPieces.cend(); ++it) {
    if (_board(*it)->legal_capture_shape(*it, king) == true) {
      position = length_direction(*it, king);
	if (path_clear(*it, position) == true) {
	  return true;
	}
      }
  }
  return false;
}
//does all of make_move but for hypothetical copy of board to test potential moves but not actually make them
bool Chess::test_move( Board b, std::pair<char, char> start, std::pair<char, char> end) const {
  std::pair <char, int> len_dir;
  start.first = toupper(start.first);
  end.first = toupper(end.first);

  if (_board(start) == NULL) {
    return false;
  }
  
  if (_board(end) != NULL) {
    if (_board(end) -> is_white() == _board(start)->is_white()) {
     
      return false;
    }
    else {
      if (_board(start)->legal_capture_shape(start, end) == false) {
        return false;
      }
    }
  }
  else if (_board(start)->legal_move_shape(start, end) == false) {
    return false;
  }
  if (_board(start)->to_ascii() != 'N' || _board(start)->to_ascii() != 'n' ||
      _board(start)->to_ascii() != 'K' || _board(start)->to_ascii() != 'k') {
    len_dir = length_direction(start, end);
    if (test_path_clear(b, start, len_dir) == false) {
      return false;
    }
  }
  if(_board(end) != NULL){
    b.remove_piece(end);
  }
  b.add_piece(end, _board(start)->to_ascii());
  b.remove_piece(start);
  if (testCheck(b, _turn_white) == true) {
    return false;
  }
  return true;

}
//does all of path_clear but for hypothetical copy of board to test potential paths
bool Chess::test_path_clear(Board b, std::pair<char, char> start, std::pair<char, int> lendir) const {
  std::pair<char, char> loc;
  char dir;
  int len;
  switch(lendir.first) {

  case 'n':
    if (start.second + lendir.second > '8') {
      return false;
    }
    for (int index = start.second + 1; index <= start.second + lendir.second - 1;
 index++) {
      loc = std::make_pair(start.first, index);
      if (b(loc) != NULL) {
        return false;
      }
    }
    break;

  case 's':
    if ((start.second - lendir.second) < '1') {
      return false;
    }
    for (int index = start.second - 1; index >= start.second - lendir.second + 1;index--) {
      loc = std::make_pair(start.first, index);
      if (b(loc) != NULL) {
        return false;
      }
    }
    break;

  case 'e':
    if ((start.first + lendir.second) > 'H') {
      return false;
    }
    for (char index = start.first + 1; index <= start.first + lendir.second - 1; index++) {
      loc = std::make_pair(index, start.second);
      if (b(loc) != NULL) {
        return false;
      }
    }
    break;

  case 'w':
    if ((start.first - lendir.second) < 'A') {
      return false;
    }
    for (char index = start.first - 1; index >= start.first - lendir.second + 1; index--) {
      loc = std::make_pair(index, start.second);
      if (b(loc) != NULL) {
        return false;
      }
    }
    break;

  case 'r':
    if (start.first + lendir.second > 'H') {
      return false;
    }
    for (int index = 1; index <= lendir.second - 1; index++) {
      dir = (start.first + index);
      len = (start.second + index);
      loc = std::make_pair(dir, len);
      if (b(loc) != NULL) {
        return false;
      }
    }
   break;

  case 'b':
    if (start.second - lendir.second < '1') {
      return false;
    }
    for (int index = 1; index <= lendir.second - 1; index++) {
      dir = (start.first + index);
      len = (start.second - index);
      loc = std::make_pair(dir, len);
      if (b(loc) != NULL) {
        return false;
      }
    }
    break;

  case 'l':
    if (start.second - lendir.second < '1') {
      return false;
    }
    for (int index = 1; index <= abs(lendir.second) - 1; index++) {
      dir = (start.first - index);
      len = (start.second - index);
      loc = std::make_pair(dir, len);
      if (b(loc) != NULL) {
        return false;
      }
    }
    break;

  default:
    if (start.second + lendir.second > '8') {
      return false;
    }
    for (int index = 1; index <= lendir.second - 1; index++) {
      dir = (start.first - index);
      len = (start.second + index);
      loc = std::make_pair(dir, len);
      if (b(loc) != NULL) {
        return false;
      }
    }
    break;
  }
  
  return true;
}

//does all of in_check but for hypothetical copy of the board
bool Chess::testCheck (Board b, bool white) const {
  std::pair<char, int> testPosition;
  std::pair<char, char> position;
  std::pair<char, char> king;
  std::vector<std::pair<char, char>> otherPieces;
  
  for (char letter = 'A'; letter <= 'H'; letter++) {
    for (char num = '8'; num >= '1'; num--) {
      position = std::make_pair(letter, num);
      if (b(position) != NULL) {
        if (b(position)->is_white() != white){
          otherPieces.push_back(position);
        }
        else if (b(position)->to_ascii() == 'K' || b(position)->to_ascii() == 'k') {
        king = position;
        }
      }
    }
  }
  for (std::vector<std::pair<char, char>>::const_iterator it = otherPieces.cbegin();
       it != otherPieces.cend(); ++it) {
    if (b(*it)->legal_capture_shape(*it, king) == true) {
      testPosition = length_direction(*it, king);
      if(b(*it)->to_ascii() != 'K' && b(*it)->to_ascii() != 'k' &&  b(*it)->to_ascii() != 'P' &&  b(*it)->to_ascii() != 'p' &&  b(*it)->to_ascii() != 'N' && b(*it)->to_ascii() != 'n'){
	if (test_path_clear(b, *it, testPosition) == true) {
	  
	  return true;
	}
      }
      else {
	return true;
      }
 
    }
  }
  return false;
  
}
//checks if in checkmate
bool Chess::in_mate( bool white ) const
{
  std::pair<char, char> position;
  std::vector<std::pair<char, char>> otherPieces;
  Board copyBoard(_board);
  //iterate through board if in check to accumulate vector of all of same team's pieces
  if(in_check(white)) {
    for( char letter = 'A'; letter <= 'H'; letter++){
      for(char num = '8'; num >= '1'; num--){
	position = std::make_pair(letter, num);
	if(_board(position) != NULL){
	  if(_board(position)->is_white() == _turn_white){
	    otherPieces.push_back(position);
	  }
	}
      }
    }
    //iterate through said vector to check if any valid moves exist for any of the team's pieces
    for(std::vector<std::pair<char, char>>::iterator it = otherPieces.begin(); it != otherPieces.end(); it++){
      for(char let = 'A'; let <= 'H'; let++){
	for( char numb = '8'; numb >= '1'; numb--){
	  position = std::make_pair(let, numb);
	  if(test_move(copyBoard, *it, position)) {
	    return  false;
	  }
	} 
      }
      
    }
    return true;
    
  }
  return false;
}
//checks if in stalemate
bool Chess::in_stalemate( bool white ) const
{
  std::pair<char, char> position;
  std::vector<std::pair<char, char>> otherPieces;
  Board copyBoard(_board);
  //iterate through board if not in check to accumulate vector of all of same team's pieces   
  if(in_check(white) == false) {
    for( char letter = 'A'; letter <= 'H'; letter++){
      for(char num = '8'; num >= '1'; num--){
        position = std::make_pair(letter, num);
        if(_board(position) != NULL){
          if(_board(position)->is_white() == white){
            otherPieces.push_back(position);
          }
        }
      }
    }
    //iterate through said vector to check if any valid moves exist for any of the team's pieces
    for(std::vector<std::pair<char, char>>::iterator it = otherPieces.begin(); it != otherPieces.end(); ++it){
      for(char let = 'A'; let <= 'H'; let++){
        for( char numb = '8'; numb >= '1'; numb--){
          position = std::make_pair(let, numb);
          if(test_move(copyBoard, *it, position)) {
            return  false;
          }
        }
      }
      
    }
    return true;

  }
  return false;
}

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator << ( std::ostream& os , const Chess& chess )
{
	// Write the board out and then either the character 'w' or the character 'b', depending on whose turn it is
	return os << chess.board() << ( chess.turn_white() ? 'w' : 'b' );
}

std::istream& operator >> ( std::istream& is , Chess& chess )
{
  //clear board of all existing pieces
  std::pair<char, char> position;
  for (char letter = 'A'; letter <= 'H'; letter++) {
    for (char num = '8'; num >= '1'; num--) {
      position = std::make_pair(letter, num);
      if (chess._board(position) != NULL) {
	chess._board.remove_piece(position);
      }
    }
  }
  //iterate through text file to add piece at given location to board
  char piece;
  for (char num = '8'; num >= '1'; num--) {
    for (char letter = 'A'; letter <= 'H'; letter++) {
      position = std::make_pair(letter, num);
      is >> piece;
      if (piece != '-') {
        chess._board.add_piece(position, piece);
      }
    }
  }
  //get last character of text file to determine who's turn it is.
  is >> piece;
  if (piece == 'w') {
    chess._turn_white = true;
  }
  else {
    chess._turn_white = false;
    }
  return is;
}
