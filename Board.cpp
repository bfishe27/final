#include <iostream>
#include <utility>
#include <map>
#ifndef _WIN32
#include "Terminal.h"
#endif // !_WIN32
#include "Board.h"
#include "CreatePiece.h"
#include <locale>

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Board::Board( void ){}

//Destructor of Board's pointers to Pieces
Board::~Board( void ) {
  std::pair<char, char> pos;
  //iterates through map of Board and deletes the pointer to pieces
  for (std::map<std::pair<char, char>, Piece*>::iterator it = _occ.begin(); it != _occ.end(); ++it) {
    delete it->second; 
  }
 
}
//Overrides () operator to return const pointer to the piece at input location
//if it exists, if not return NULL
const Piece* Board::operator()( std::pair< char , char > position ) const
{
  if(_occ.find(position) != _occ.end()) {
    //piece at the prescribed position
    return _occ.at(position);
    
  }
  return NULL;
}

//Copy constructor of Board object
Board::Board(const Board& b) {

  std::pair<char, char> pos;
  char pRep;
  //iterates through Board b's map of Piece* and pair<char, char> and copies
  //them into the copy map of b
  for(std::map<std::pair<char, char>, Piece*>::const_iterator it = b._occ.cbegin(); it != b._occ.cend(); ++it) {
    pos = it->first;
    pRep = it->second->to_ascii();
    add_piece(pos, pRep);
  }
  
}

//Tries to add new piece of described char representation to entered location
//returns false if not valid location, designator, or piece already at location, true
//otherwise
bool Board::add_piece( std::pair< char , char > position , char piece_designator )
{
  //if location not on board, false
  if (position.first < 'A' || position.second < '1' || position.first > 'H' || position.second > '8') {
    return false;
  }
  //if position not in map, false
  if (_occ.find(position) != _occ.end()) {
    if(_occ.at(position) == NULL) {
      return false;
    }
  }
  char lowerPD = tolower(piece_designator);
  //if piece designator not king, queen, knight, pawn, bishop, or rock, false
  if ((lowerPD != 'k') && (lowerPD != 'q') && (lowerPD != 'b') && (lowerPD != 'n') && (lowerPD != 'r') && (lowerPD != 'p') && (lowerPD != 'm')) {
    return false;
  }
  //otherwise, add piece to location and return true
  _occ[ position ] = create_piece( piece_designator );
  return true;
}

//removes a piece at a specified location if there's a piece there
bool Board::remove_piece(std::pair< char, char> position) {
  //if piece at location in board map deletes pointer to piece and frees memory
  if (_occ[position]) {
    delete _occ[position];
    _occ.erase(position);
    return true;
  }
  return false;
}

//Returns true if board has right number of kings on it
bool Board::has_valid_kings( void ) const
{
  int count=0;
  //iterates through the board and checks if piece is a king, and if it is, count++
  for(std::map<std::pair<char, char>, Piece*>::const_iterator it = _occ.begin(); it != _occ.end(); ++it) {
    char pieceChar =it->second->to_ascii();
    if ((pieceChar == 'k') || (pieceChar=='K')) {
      count++;
    }

  }
  //if less or more than 2 kings on the board, returns false
  if (count !=2) {
    return false;
  }
  return true;
}

//Prints board out to standard out
void Board::display( void ) const
{
  //Sets the foreground of the chess board to red
  Terminal::color_fg(true, Terminal::Color::RED);
  std::cout << *this;
  //sets the rest of the text to normal lettering and color
  Terminal::set_default();
}

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator << ( std::ostream& os , const Board& board )
{
	for( char r='8' ; r>='1' ; r-- )
	{
		for( char c='A' ; c<='H' ; c++ )
		{
			const Piece* piece = board( std::pair< char , char >( c , r ) );
			if( piece ) os << piece->to_ascii();
			else        os << '-';
		}
		os << std::endl;
	}
	return os;
}
