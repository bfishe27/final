//cpp file for Knight

#include "Knight.h"
#include <utility>
#include <locale>

using std::pair;

bool Knight::legal_move_shape(pair<char, char> start, pair<char, char> end) const {
  //check if moving
            if ((start.first == end.first) && (start.second == end.second)) {
            return false;
          }
	    //check bounds
            if (start.first < 'A'){
	      return false;
	    }
	    if ( end.first < 'A'){
	      return false;
	    }
	    if( start.first > 'H')
	      return false;
	    if (end.first > 'H')
              {
                return false;
              }
            if (start.second < '1' || end.second < '1' || start.second > '8' || end.second > '8') {
              return false;
            }
	    //check moving in L 
	    if (abs(end.first - start.first) == 2) {
	      if (abs(end.second - start.second)==1){
		return true;
	      }
	    }
	    else if (abs(end.first - start.first) == 1) {
              if (abs(end.second - start.second)==2){
		return true;
	      } 
            }
  
  return false;
}

bool Knight::legal_capture_shape(pair<char, char>start, pair<char, char> end) const {
  //same moves are legal
  return legal_move_shape(start, end);
}
